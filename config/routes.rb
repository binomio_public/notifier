if Notifier.testing_routes_enabled
  Rails.application.routes.draw do
    get '/notifier/send-notification' => 'notifier/notifications#send_notification', as: :send_notification
    get '/notifier/raise-exception' => 'notifier/notifications#raise_exception', as: :raise_exception
  end
end