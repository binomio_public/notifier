# Notifier
This gem provides an API to notify an external server whenever certain events (typically errors) occur. Check the wiki for details.