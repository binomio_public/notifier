require "net/http"

require "notifier/version"
require "notifier/engine"

module Notifier

  mattr_accessor :monitor_url, :monitor_app_codename, :monitor_enabled, :testing_routes_enabled

  # Method to be called on server initialization to configure this gem
  def self.setup
    yield self
    [:monitor_url, :monitor_app_codename, :monitor_enabled, :testing_routes_enabled].each do |key|
      raise "Notifier gem has a missing configuration key: #{key}." if self.send(key).nil?
    end
    raise "Expected 'monitor_enabled' to be a boolean." unless self.monitor_enabled.in? [true, false]
    raise "Expected 'testing_routes_enabled' to be a boolean." unless self.testing_routes_enabled.in? [true, false]
    raise "Expected 'monitor_app_codename' to be 1-32 alphanumeric characters or underscores." unless self.monitor_app_codename =~ /\A\w{1,32}\z/
    raise "Expected 'monitor_url' to be a valid URL." unless self.monitor_url =~ /\Ahttps?:\/\//
  end

  # Given an exception, and optionally the request object from the controller
  # where it arose and the current user, report the detailed error
  def self.exception exception, request = nil, user = nil
    message = "#{exception.class.name}: #{exception.message}"
    begin # Attempt to add additional information to the error message
      message += " @ #{request.method} #{request.path}" if request.present?
      message += " by user #{user.id}" if user.present?
    rescue
    end
    puts exception.backtrace.join("\n")
    return self.error message
  end

  # Report an error
  def self.error message
    puts "#{DateTime.now.iso8601} Notifier.error: #{message}"
    return send_message message, "error"
  end

  # Report a warning
  def self.warning message
    puts "#{DateTime.now.iso8601} Notifier.warning: #{message}"
    return send_message message, "warning"
  end

  private

  # Sends a message to the monitor. Returns false if the message could not be
  # sent for any reason, or true otherwise.
  def self.send_message message, type
    raise "Notifier gem has not been configured." if monitor_enabled.nil?
    unless monitor_enabled
      puts "Notification was not sent because monitor is disabled."
      return false
    end
    begin
      uri = URI.parse(monitor_url)
      https = Net::HTTP.new(uri.host, uri.port)
      https.use_ssl = monitor_url.start_with? "https:"
      req = Net::HTTP::Post.new((uri.path.present? ? uri.path : "/"), 'Content-Type' => 'application/json')
      req.body = {
        codename: monitor_app_codename,
        message: message,
        type: type
      }.to_json
      res = https.request(req)
      raise "Unexpected status code #{res.code} received from monitor" if res.code != "200"
      puts "Response from monitor: #{res.body}"
      return true
    rescue => ex
      puts "#{DateTime.now.iso8601} Notifier: Failed to send monitor notification: #{ex.message}"
      puts ex.backtrace.join("\n")
      return false
    end
  end
end