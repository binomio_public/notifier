module Notifier

  class NotificationsController < ApplicationController

    # Trigger the sending of a test notification
    def send_notification
      sending_successful = nil
      if params[:type] == "warning"
        sending_successful = Notifier.warning "This is a test warning notification"
      else
        sending_successful = Notifier.error "This is a test error notification"
      end
      if Notifier.monitor_enabled
        render plain: sending_successful ? "Notification sent" : "An error occurred when sending the notification"
      else
        render plain: "Notification was not sent because monitor is disabled"
      end
    end

    # Raise an unhandled exception to test how the application reacts
    def raise_exception
      raise "This exception was deliberately raised to test controller exception handling"
    end

  end
end