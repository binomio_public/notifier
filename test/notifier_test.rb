require "test_helper"
require 'socket'

Notifier.setup do |conf|
  conf.monitor_url = "http://localhost:3000/messages"
  conf.monitor_app_codename = "test1"
  conf.monitor_enabled = true
  conf.testing_routes_enabled = true
end

class NotifierTest < Minitest::Test

  def setup
  end

  def test_it_sends_error
    assert_equal true, Notifier.error("This is a test error (#{SecureRandom.hex(8)})")
  end

  def test_it_sends_warning
    assert_equal true, Notifier.warning("This is a test warning (#{SecureRandom.hex(8)})")
  end
end
